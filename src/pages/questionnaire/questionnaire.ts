import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the QuestionnairePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-questionnaire',
  templateUrl: 'questionnaire.html',
})
export class QuestionnairePage {
  questions: Array<any> = [{
    text: 'Who am I contracting with?',
    rate: 1
  }, {
    text: 'Where is the enterprise located?',
    rate: 1
  }, {
    text: 'What specific rights come with my investment?',
    rate: 1
  }, {
    text: 'Are there financial statements? If so, are they audited, and by whom?',
    rate: 1
  }];

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad QuestionnairePage');
  }

  public submit() {
    this.alertCtrl.create({
      title: 'Congratulations!',
      message: 'You have completed the questionnaire.',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: data => {
            this.navCtrl.pop();
          }
        }
      ]
    }).present();
  }
}
