import {Component, ViewChild} from '@angular/core';
import {NavController, AlertController, Button} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { Util } from '../../app/util';
import { CurrencyDetailPage } from '../currency-detail/currency-detail';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public static histodayAPIL: string = 'https://min-api.cryptocompare.com/data/histoday';
  public static priceAPI: string = 'https://min-api.cryptocompare.com/data/price';
  public static newsAPI: string = 'https://min-api.cryptocompare.com/data/v2/news/';

  interestList: Array<object> = [
    {sym: 'BTC', avePrice: 0},
    {sym: 'ETH', avePrice: 0}
  ];

  constructor(public navCtrl: NavController, public httpClient: HttpClient, private alertCtrl: AlertController, private storage: Storage) {
    storage.get('interestList').then((val) => {
      if (val != null) {
        this.interestList = val;
        // Update
        for (let ele of this.interestList) {
          this.getAvePrice(ele);
        }
      }
      // console.log(this.interestList);
    })

    for (let ele of this.interestList) {
      this.getAvePrice(ele);
    }
  }

  getAvePrice(obj: object) {
    let para : object = {fsym: obj['sym'], tsyms: 'AUD'};

    // console.log(Util.buildURL(this.averageAPI, obj));
    
    this.httpClient.get(Util.buildURL(HomePage.priceAPI, para))
    .subscribe(data => {
      if (data['Response'] === 'Error') {
        this.alertCtrl.create({
          title: 'Wrong symbol for currency',
          message: 'Please check your symbol',
          buttons: [
            {
              text: 'OK',
              role: 'cancel'
            }
          ]
        }).present();
        this.deletePress(obj);
        return;
      }
      obj['avePrice'] = data['AUD'];
      // console.log(this.interestList);
    })
  }

  public static getCurrentPrice(obj: object, httpClient: HttpClient) {
    let para : object = {fsym: obj['sym'], tsyms: 'AUD'};
    httpClient.get(Util.buildURL(HomePage.priceAPI, para))
    .subscribe(data => {
      if (data['Response'] === 'Error') {
        console.log('get price error');
      }
      obj['avePrice'] = data['AUD'];
    });
  }

  public addButtonPress() {
    let alert = this.alertCtrl.create({
      title: 'Add tracker by symbol',
      cssClass: 'addTrackerAlter',
      inputs: [{
        name: 'sym'
      }],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Add',
          handler: data => {
            this.addCryptoCurrency(data['sym']);
          }
        }
      ]
    });
    alert.present();
  }

  addCryptoCurrency(sym: string) {
    sym = sym.toUpperCase();
    for (let ele of this.interestList) {
      if (ele['sym'] == sym) {
        return;
      }
    }
    console.log(sym);
    
    
    let obj: object = {'sym': sym, avePrice: 0}
    this.interestList.push(obj);
    this.getAvePrice(obj);
    this.storeList();
  }

   public deletePress(item: object) {
    let index = this.interestList.lastIndexOf(item)
    if (index > -1) {
      this.interestList.splice(index, 1);
      // console.log(this.interestList);
    }
    this.storeList();
  }

  storeList() {
    this.storage.set('interestList', this.interestList);
  }

  public gotoDetail(item: object) {
    this.navCtrl.push(CurrencyDetailPage, item);
  }
}
