import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { HomePage } from '../home/home';
import { Util } from '../../app/util';
import { Chart } from "chart.js";

/**
 * Generated class for the CurrencyDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-currency-detail',
  templateUrl: 'currency-detail.html',
})
export class CurrencyDetailPage {
  @ViewChild('histodayChart') histodayChart;
  @ViewChild('priceTag') priceTag;
  symbol: string;
  info: object;
  histodayData: Array<any>;
  lastPrice: number;
  private _priceCheckTaskId: any;
  private _priceColorTaskId: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public httpClient: HttpClient) {
    this.symbol = navParams.get('sym');
    this.info = navParams.data;
    HomePage.getCurrentPrice(this.info, httpClient);
    this.getHistoday();
    
    this._priceCheckTaskId = setInterval(function(info: object) {
      // console.log(info);
      HomePage.getCurrentPrice(info, httpClient);
    }, 60 * 1000, this.info);
  }

  ionViewDidLoad() {
    this._priceCheckTaskId = setInterval(function(last, current, tag) {
      if (last > current) {
        tag.nativeElement.style.color = 'red';
      } else {
        tag.nativeElement.style.color = 'green';
      }  
    }, 1 * 100, this.lastPrice, this.info['avePrice'], this.priceTag);
  }

  ionViewWillLeave() {
    clearInterval(this._priceCheckTaskId);
    clearInterval(this._priceColorTaskId);
  }

  getHistoday() {
    let para: object = {fsym: this.symbol, tsym: 'AUD', limit: 30}

    this.httpClient.get(Util.buildURL(HomePage.histodayAPIL, para))
    .subscribe(data => {
      if (data['Response'] === 'Error') {
        
        return;
      }
      this.histodayData = data['Data'];
      this.drawChart();
    })
  }

  drawChart() {
    // console.log(this.histodayChart);
    let closest = this.histodayData[0];
    let timeMax = this.histodayData[0].time, timeMin = this.histodayData[0].time;
    let labelTime = [], highs = [], lows = [], opens = [], closes = [];
    let cTime = new Date().getTime() / 1000;
    for (let element of this.histodayData) {
      if (element.time > timeMax) {
        timeMax = element.time;
      }
      if (element.time < timeMin) {
        timeMin = element.time;
      }
      if (Math.abs(cTime - element.time) < Math.abs(cTime - closest.time)) {
        closest = element;
      }
      let timeInfo = new Date(element.time * 1000).toDateString().split(' ');      
      labelTime.push(timeInfo[1] + ' ' + timeInfo[2]);
      highs.push(element.high);
      lows.push(element.low);
      opens.push(element.open);
      closes.push(element.close);
    }
    this.lastPrice = closest.open;
    let chart = new Chart(this.histodayChart.nativeElement, {
      // The type of chart we want to create
      type: 'line',
  
      // The data for our dataset
      data: {
        labels: labelTime,
          
        datasets: [{
          label: 'Price',
          // backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
          // borderColor: window.chartColors.red,
          fill: false,
          data: highs,
        }]
      },
      options: {
        scales: {
          xAxes: [{
              time: {
                unit: 'day'
              },
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
          }]
        }
      }
    });
  }
}
