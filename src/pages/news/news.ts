import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Util } from '../../app/util';
import { HomePage } from '../home/home';

/**
 * Generated class for the NewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {
  newsData: Array<any>;
  regulationOnly: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private httpClient: HttpClient) {
    this.getNewsData();
    
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad NewsPage');
  }

  getNewsData() {
    let para : object = {lang: 'EN'};
    this.httpClient.get(Util.buildURL(HomePage.newsAPI, para))
    .subscribe(data => {
      if (data['Response'] === 'Error') {
        console.log('get news error');
      }
      this.newsData = data['Data'];
    });
  }

  public openNews(news: any) {
    window.open(news.url, '_system', 'location=yes');
  }

  public showRegulationOnly(news: any): boolean {
    // console.log('checking');
    if (news === undefined) {
      return false;
    }
    
    if (this.regulationOnly) {
      return news.categories.includes('Regulation');
    } else {
      return true;
    }
  }

  public refreshList() {

  }
}
