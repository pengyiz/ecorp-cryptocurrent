import { Component } from '@angular/core';

export class Util {
    public static buildURL(base : string, para : object) {
        let url : string = base + '?';
        for (let key in para) {
            url += key + '=' + para[key] + '&';
        }
        return url.substring(0, url.length - 1);
    }

    public static objectToArray(obj: Object) {
        let a : Array<string> = [];
        for (let key in obj) {
            a.push(key);
        }
        return a;
    }
}